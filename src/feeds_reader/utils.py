import feedparser
import json

from datetime import datetime
from dateutil.parser import parse
from dateutil.tz import gettz
from pytz import timezone as tz

from django.utils import timezone
from django.db.models import Q

from catalyst_rss_worker.settings import TIME_ZONE
from .models import FeedEntry


'''
Helper function which fetches feeds from source
'''
def fetch_feeds_from_source(feed_source) :
    current_time = timezone.now() # Current time before fetch start
    ''' If fetching for first time, Fetch all data '''
    if feed_source.last_fetched_at :
        feed = feedparser.parse(feed_source.source_link, modified=feed_source.last_fetched_at)
    else :
        feed = feedparser.parse(feed_source.source_link)
    feed_entries = []
    for entry in feed.entries :
        feed_entries.append(
            FeedEntry(
                title = entry.title,
                description = entry.summary,
                published_at = parse(entry.published),
                post_link = entry.link,
                feed_source_id = feed_source.id
            )
        )
    FeedEntry.objects.bulk_create(feed_entries, ignore_conflicts=True)

    ''' updating last_fetched_at for the feed source '''
    feed_source.last_fetched_at = current_time
    feed_source.save(update_fields=('last_fetched_at',))



Q_CONDITIONS = {
    'OR' : Q.OR,
    'AND' : Q.AND
}

STRING_OPERATORS = {
    'equal' : 'iexact',
    'contains' : 'icontains',
    'begins_with' : 'istartswith'
}

def get_query_object(condition, data, q_objects=Q()) :

    for rule in data :
        if rule.get('condition'):
            condition = Q_CONDITIONS[rule['condition']]
            q_objects = get_query_object(condition, rule['rules'], q_objects)
        else :
            key = rule['field']+ '__' + STRING_OPERATORS[rule['operator']]
            q_objects.add(
                Q(**{key : rule['value']}),
                condition
            )
    return q_objects