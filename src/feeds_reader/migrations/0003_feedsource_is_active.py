# Generated by Django 3.0.5 on 2020-06-27 15:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feeds_reader', '0002_auto_20200627_1948'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedsource',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
    ]
