import json

from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q

from .models import FeedEntry
from .utils import get_query_object, Q_CONDITIONS


@csrf_exempt
def index(request) :
    if request.method == 'POST' :
        # validation
        queryset = FeedEntry.objects
        data = json.loads(request.body)
        q_objects = get_query_object(Q_CONDITIONS[data['condition']], data['rules'], Q())
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        no_of_feeds = queryset.count()
        return render(request, 'feeds_reader/feeds-count.html', {'no_of_feeds' : no_of_feeds})

    no_of_feeds = FeedEntry.objects.count()
    return render(request, 'feeds_reader/index.html', {'no_of_feeds' : no_of_feeds})