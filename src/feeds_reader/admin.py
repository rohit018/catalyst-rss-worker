from django.contrib import admin
from .models import FeedSource, FeedEntry


@admin.register(FeedSource)
class FeedSourceAdmin(admin.ModelAdmin) :
    list_display = ('source_link', 'last_fetched_at', 'is_active')
    list_filter = ('is_active', )
    search_fields = ('source_link',)
    ordering = ('-last_fetched_at',)


@admin.register(FeedEntry)
class FeedEntryAdmin(admin.ModelAdmin) :
    list_display = ('title', 'post_link', 'published_at',)
    ordering = ('-published_at',)
    search_fields = ('title', 'feed_source')
    date_hierarchy = 'published_at'
