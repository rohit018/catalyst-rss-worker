from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver



'''
Stores information about feed sources in the database
'''
class FeedSource(models.Model) :
    source_link = models.URLField()
    last_fetched_at = models.DateTimeField(null=True, blank=True, editable=False)
    is_active = models.BooleanField(default=True)

    class Meta :
        db_table = 'feed_source'

    def __str__(self) :
        return self.source_link


@receiver(post_save, sender=FeedSource)
def initial_feed_dump(sender, **kwargs) :
    feed_source = kwargs['instance']
    if kwargs['created']:
        from .utils import fetch_feeds_from_source
        fetch_feeds_from_source(feed_source)


'''
Store information about feed data in the database
'''
class FeedEntry(models.Model) :
    title = models.CharField(max_length=250)
    description = models.TextField()
    published_at = models.DateTimeField()
    post_link = models.URLField(max_length=250, unique=True)
    feed_source = models.ForeignKey(FeedSource, on_delete=models.CASCADE) # Soft delete is preferred

    class Meta : 
        db_table = 'feed_entry'
        verbose_name = 'Feed Entry'
        verbose_name_plural = 'Feed Entries'

    def __str__(self) :
        return self.title
    