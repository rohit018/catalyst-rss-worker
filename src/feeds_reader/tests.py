from django.test import TestCase
from django.utils import timezone
from .models import FeedSource, FeedEntry
from .tasks import fetch_latest_rss_feeds
from .utils import fetch_feeds_from_source


class FeedTestCase(TestCase):
    def setUp(self):
        self.feed_source = FeedSource.objects.create(source_link='https://www.indiatoday.in/rss/1206514')

    def test_feed_entries_create(self):
        feed_source = FeedSource.objects.create(source_link="https://www.indiatoday.in/rss/1206584")
        feed_entries_exist = FeedEntry.objects.filter(feed_source=feed_source).exists()
        self.assertNotEqual(feed_source.id, None)
        self.assertEqual(feed_entries_exist, True)

    def test_fetch_latest_rss_feeds(self) :
        current_time = timezone.now()
        fetch_latest_rss_feeds()
        assertGreaterEqual(self.feed_source.last_fetched_at, current_time)

    def test_fetch_feeds_from_source(self) :
        current_time = timezone.now()
        test_fetch_feeds_from_source(self.feed_source)
        assertGreaterEqual(self.feed_source.last_fetched_at, current_time)
