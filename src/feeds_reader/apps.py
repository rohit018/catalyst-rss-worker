from django.apps import AppConfig


class FeedsReaderConfig(AppConfig):
    name = 'feeds_reader'
