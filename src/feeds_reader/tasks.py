from celery import shared_task 

from .models import FeedSource
from .utils import fetch_feeds_from_source


''' 
Shared task which runs every 15 minutes to fetch 
latest data from sources
'''
@shared_task
def fetch_latest_rss_feeds() :
     feed_sources = FeedSource.objects.filter(is_active=True)
     for source in feed_sources :
          fetch_feeds_from_source(source)
