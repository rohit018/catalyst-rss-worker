from django import template
from catalyst_rss_worker.settings import CELERY_URL

register = template.Library()

@register.simple_tag
def celery_url():
    return CELERY_URL