from __future__ import absolute_import
import os
from celery import Celery
from celery.schedules import crontab

from django.conf import settings

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'catalyst_rss_worker.settings')
app = Celery('catalyst_rss_worker')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.beat_schedule = {
    'fetch-latest-feeds-every-15-minutes': {
        'task': 'feeds_reader.tasks.fetch_latest_rss_feeds',
        'schedule': crontab(minute='*/15'),
    },
}
