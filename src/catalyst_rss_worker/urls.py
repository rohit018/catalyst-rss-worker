from django.contrib import admin
from django.urls import path, include, reverse
from django.conf.urls.static import static
from django.shortcuts import redirect

from . import settings


admin.site.site_title = "Catalyst RSS Worker"
admin.site.site_header = "Catalyst RSS Worker"

def index(request) :
    return redirect(reverse('query-builder'))

urlpatterns = [
    path('', index),
    path('admin/', admin.site.urls),
    path('query/', include('feeds_reader.urls'))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
