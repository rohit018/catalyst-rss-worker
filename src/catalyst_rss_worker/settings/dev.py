import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

SECRET_KEY = 'z4i9vxlm-c#ba+3%32e4ni0x590g7=fv3ihm4rwmhf3la8i9t#'
DEBUG = True

ALLOWED_HOSTS = ['127.0.0.1', 'localhost']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'catalyst_rss_worker',
        'USER': 'catalyst',
        'PASSWORD': '50meR@nd0m3p@55',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}


BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = 'redis://localhost:6379'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Asia/Kolkata'
CELERY_URL ='http://127.0.0.1:8000'