### Catalyst RSS Worker guide ###

* Catalyst RSS worker sync latest feeds from feed sourcs

### How do I get set up? ###

* To install this app on your machine, You need to have docker and docker-compose engine installed on your machine. for more info, visit https://docs.docker.com/get-docker/
* docker-compose build && docker-compose up 
* At first, Django related containers may fail because no database is yet created in postgres container.
* Use 'docker exec -it <db_container_name> bash' command to open container's terminal. Create database inside container. 
* Once done, use following command to get application running 'docker-compose build && docker-compose up'.
* Create a superuser for our application.

### How to use application ###
* Create a superuser
* Login with admin panel
* Create RSS source, Once created it will initially ftech rss entries and dump into database.
* A task will run after every 15 minutes to add new entries
